<?php
ini_set('display_errors',1); 
 error_reporting(E_ALL);
include 'Functions.php'; 
$result = "UNDEFINED"; 
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$req = $_POST['request']; 
	switch($req)
	{
	case "RunStatement":
		$result = RunSQLStatement($_POST['Statement']); 
		break;
	case "AddFocusTest":
		$result = AddFocusTest($_POST['EndFocusValue'],$_POST['EndFocusStep'],$_POST['FocusTime'],$_POST['FoundCore'],$_POST['AverageFrameLatency'],$_POST['MaxFrameLatency'],$_POST['ImageName'],$_POST['Battery'],$_POST['Temperature']); 
		break;
	case "AddStep":
		$result = AddStep($_POST['FocusID'],$_POST['Step'],$_POST['TotalWaited'],$_POST['FocusValue']); 
		break;
	case "AddAnalysisTest":
		$result = AddAnalysisTest($_POST['FocusID'],$_POST['TimeTaken'],$_POST['DefectCount'],$_POST['DefectPixelCount'],$_POST['Focused'],$_POST['AnalysisPassed'],$_POST['Centered']); 
		break;
		case "UploadFile"
		$result = UploadFile($_FILES['file']);
	default: 
		$result = "unknown request: " . $req; 
	}// end switch on request
}
echo $result; 
?>